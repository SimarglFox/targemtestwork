#include "stdafx.h"
#include "myString.h"
#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
    std::vector<myString> stringsArr;
    bool exit_loop = false;
    std::cout << "How ,any strings you wanna enter?" << std::endl;
    int strings_count;
    std::cin >> strings_count;
    for (int i = 0; i < strings_count; i++)
    {
        std::cout << "enter string" << std::endl;
        myString input_string;
        std::cin >> input_string;
        stringsArr.push_back(input_string);
    }

    std::sort(stringsArr.begin(), stringsArr.end());

    for (auto string : stringsArr)
    {
        std::cout << string << std::endl;
    }

    system("pause");
    return 0;
}

