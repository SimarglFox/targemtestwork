#pragma once
#include <string.h>
#include <iostream>

class myString
{
public:
    myString();
    myString(const char *string);
    myString(const myString& input_string);
    ~myString();

    myString& operator = (const myString& input_string);
    myString& operator += (const char *input_string);
    myString& operator += (const myString& input_string);

    const char* get_c_str() const { return c_str; }
    size_t size() const { return string_size; }

private:
    char* change_size(size_t new_size);
    void copy_from(const myString& input_string);
    myString& merge_string(const char* input_string);
private:
    char *c_str{nullptr};
    size_t string_size;
};

myString operator + (const myString& first_string, const myString& second_string);

bool operator > (const myString first, const myString second);
bool operator < (const myString first, const myString second);
std::ostream& operator <<(std::ostream& output_stream, const myString& output_string);
std::istream& operator >>(std::istream& input_stream, myString& input_string);