#include "stdafx.h"
#include "myString.h"
#include <stdlib.h>

myString::myString()
{
    string_size = 0;
    c_str = new char;
    *c_str = 0;
}

myString::myString(const char *string)
{
    string_size = strlen(string);
    c_str = new char[string_size + 1];
    c_str[string_size] = 0;
    if (string_size == 0)
    {
        return;
    }
    strcpy_s(c_str, string_size + 1, string);
}

myString::myString(const myString& input_string)
{
    this->copy_from(input_string);
}

myString::~myString()
{
    if (string_size != 0)
    {
        delete[] c_str;
    }
    else
    {
        delete c_str;
    }
}

char* myString::change_size(size_t new_size)
{
    char *new_pointer = new char[new_size + 1];
    strcpy_s(new_pointer, new_size, c_str);
    return new_pointer;
}

void myString::copy_from(const myString& input_string)
{
    string_size = input_string.string_size;
    
    c_str = new char[string_size + 1];
    if (input_string.string_size == 0)
    {
        c_str[0] = 0;
        return;
    }
    strcpy_s(c_str, string_size + 1, input_string.c_str);
}

myString& myString::operator=(const myString& input_string)
{
    if (this == &input_string)
    {
        return *this;
    }
    
    copy_from(input_string);

    return *this;
}

myString& myString::merge_string(const char *input_string)
{
    char* temp_string = new char[string_size + 1];
    if (string_size != 0)
    {
        strcpy_s(temp_string, string_size + 1, c_str);
    }
    else
    {
        temp_string[0] = 0;
    }
    if (string_size != 0)
    {
        delete[] c_str;
    }
    else
    {
        delete c_str;
        string_size++;
    }
    string_size += strlen(input_string);
    c_str = new char[string_size + 1];
    snprintf(c_str, string_size, "%s%s\0", temp_string, input_string);
    return *this;
}


myString& myString::operator+=(const char *input_string)
{
    return this->merge_string(input_string);
}

myString & myString::operator+=(const myString& input_string)
{
    return this->merge_string(input_string.c_str);
}

myString operator+(const myString & first_string, const myString & second_string)
{
    auto sum{ first_string };
    sum += second_string;
    return sum;
}

bool operator>(const myString first, const myString second)
{
    return _strcmpi(first.get_c_str(), second.get_c_str()) < 0 ? true : false;
}

bool operator<(const myString first, const myString second)
{
    return _strcmpi(first.get_c_str(), second.get_c_str()) > 0 ? true : false;
}

std::ostream& operator<<(std::ostream & output_stream, const myString& output_string)
{
    return output_stream << output_string.get_c_str();
}



std::istream & operator>>(std::istream & input_stream, myString& input_string)
{
    input_string = "";
    input_stream >> std::ws;

    //������ ������, �� ������ �� �� gcount �� tellq �� ���� ��� ��������� ������
    char *ch = new char[2];
    
    while (input_stream.get(ch[0]) && !isspace(ch[0]))
    {
        ch[1] = 0;
        input_string += ch;
    }
    return input_stream;
}
